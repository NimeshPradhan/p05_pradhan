//
//  GameScene.swift
//  P05_pradhan
//
//  Created by Nimesh on 3/23/17.
//  Copyright © 2017 npradha2. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var rocket = SKSpriteNode()
    var moveAndRepositionrocket = SKAction()
    var moverocket = SKAction()
    var repositionrocket = SKAction()
    
    var astroids = SKSpriteNode()
    var moveAndRemoveastroids = SKAction()
    
    var scoreLine = SKSpriteNode()
    var restartButton = SKSpriteNode()
    
    var score = Int()
    var highScore = Int()
    
    var moveScene = SKAction()

    var nodeArray = Array<SKSpriteNode>()
    var rocketArray = Array<SKSpriteNode>()
    
    var gameOver = Bool()
    var moverockets = Bool()
    var labelGenerated = Bool()
    
    var count = Int()
    var x_origin = Int()
    var y_origin = Int()
    
    var scoreLabel = SKLabelNode()
    var highScoreLabel = SKLabelNode()
    
    var background = SKSpriteNode()
    
    var fire = SKEmitterNode()
    var moveFire = SKAction()
    var repositionFire = SKAction()
    var moveAndRepositionFire = SKAction()
    
    var explosion = SKEmitterNode()
    
    static  let blackrocket : UInt32 = 0x1 << 1
    static  let astroids : UInt32 = 0x1 << 2
    static  let scoreL : UInt32 = 0x1 << 3
    
    func startGame(){
        
        //print(UIFont.familyNames)
        self.physicsWorld.contactDelegate = self
        
        count = 0
        x_origin = (Int)(astroids.size.width)
        y_origin = 500
        
        background = SKSpriteNode(imageNamed: "background")
        background.size = self.size
        background.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        background.zPosition = 1
        self.addChild(background)
        
        self.createrocket()
        self.createScoreLine(offset: 0)
        self.createScoreLine(offset: 700.0)
        self.createScoreLine(offset: 1400.0)
        self.spawnastroids()
        self.moveScene = SKAction.moveBy(x: 0, y: -300, duration: 0.8)
        self.createScoreLabel()
        self.createHighScoreLabel()
        self.updateHighScoreLabel()
        
        fire = SKEmitterNode(fileNamed: "fire.sks")!
        fire.position = CGPoint(x: rocket.position.x + 20, y:rocket.position.y - rocket.size.height/2)
        fire.zPosition = 4
        fire.zRotation = CGFloat(M_PI)/CGFloat(5.5)
        self.addChild(fire)
        
    }
    
    override func didMove(to view: SKView) {
        // Get label node from scene and store it for use later
        self.startGame()
    }
    
    //spawn astroids at random time
    func spawnastroids(){
        let spawn = SKAction.run{
            () in
            self.createastroids(offset: 0)
            self.createastroids(offset: 700)
            self.createastroids(offset: 1400)
        }
        
        let delay = SKAction.wait(forDuration: 2, withRange: 2)
        let SpawnDelay = SKAction.sequence([spawn, delay])
        let SpawnDelayForever = SKAction.repeatForever(SpawnDelay)
        self.run(SpawnDelayForever)
    }
    
    //update high score label
    func updateHighScoreLabel(){
        if(UserDefaults.standard.object(forKey: "highScore") != nil){
            highScore = UserDefaults.standard.value(forKey: "highScore") as! Int
            highScoreLabel.text = "BEST:\(highScore)"
        }
    }
    
    //reset all variables, flags and restart game
    func restartGame(){
        labelGenerated = false
        score = 0
        count = 0
        gameOver = false
        nodeArray.removeAll()
        self.removeAllChildren()
        startGame()
    }

    //create rocket
    func createrocket(){
        self.rocket = SKSpriteNode(imageNamed: "rocket")
        self.rocket.setScale(0.3)
        self.rocket.position = CGPoint(x: self.size.width - rocket.size.width , y: 100)
        self.rocket.zPosition = 5
        self.rocket.physicsBody = SKPhysicsBody(texture: rocket.texture!,
                                           size: rocket.texture!.size())
        self.rocket.physicsBody = SKPhysicsBody(rectangleOf: rocket.size)
        self.rocket.physicsBody?.categoryBitMask = GameScene.blackrocket
        self.rocket.physicsBody?.collisionBitMask = GameScene.astroids
        self.rocket.physicsBody?.contactTestBitMask = GameScene.astroids | GameScene.scoreL
        self.rocket.physicsBody?.affectedByGravity = false
        self.rocket.physicsBody?.isDynamic = true
        
        self.addChild(self.rocket)
        
        self.moverocket = SKAction.moveBy(x: -50, y: 100, duration: 0.2)
        self.repositionrocket = SKAction.moveBy(x: 50, y: -100, duration: 0.8)
        self.moveAndRepositionrocket = SKAction.sequence([moverocket , repositionrocket])

        self.moveFire = SKAction.moveBy(x: -50, y: 100, duration: 0.2)
        self.repositionFire = SKAction.moveBy(x: 50, y: -100, duration: 0.8)
        self.moveAndRepositionFire = SKAction.sequence([moveFire, repositionFire])

    }
    
    //create astroid parameters
    func createastroids(offset: Int){
        
        let width = self.randomNumber(min: 40, max: 70)
        let height = self.randomNumber(min: 40, max: 70)
        switch (self.randomNumber(min: 1, max: 6)) {
        case 1:
            self.astroids = SKSpriteNode(imageNamed: "astroid1")
            self.astroids.size = CGSize(width: width, height: height)
            break
        case 2:
            self.astroids = SKSpriteNode(imageNamed: "astroid2")
            self.astroids.size = CGSize(width: width, height: height)
            break
        case 3:
            self.astroids = SKSpriteNode(imageNamed: "astroid3")
            self.astroids.size = CGSize(width: width, height: height)
            break
        case 4:
            self.astroids = SKSpriteNode(imageNamed: "astroid4")
            self.astroids.size = CGSize(width: width, height: height)
            break
        case 5:
            self.astroids = SKSpriteNode(imageNamed: "astroid5")
            self.astroids.size = CGSize(width: width, height: height)
            break
        case 6:
            self.astroids = SKSpriteNode(imageNamed: "astroid6")
            self.astroids.size = CGSize(width: width, height: height)
            break
        default:
            self.astroids = SKSpriteNode(imageNamed: "astroid1")
            self.astroids.size = CGSize(width: width, height: height)
            break
        }
        
        self.astroids.position = CGPoint(x: -x_origin, y: y_origin + offset)
        self.astroids.zPosition = 5
        
        self.astroids.name = "astroids"

        self.astroids.physicsBody = SKPhysicsBody(rectangleOf: astroids.size)
        self.astroids.physicsBody?.categoryBitMask = GameScene.astroids
        self.astroids.physicsBody?.collisionBitMask = GameScene.blackrocket
        self.astroids.physicsBody?.contactTestBitMask = GameScene.blackrocket
        self.astroids.physicsBody?.affectedByGravity = false
        self.astroids.physicsBody?.isDynamic = true
        
        self.addChild(self.astroids)
        let runastroids = SKAction.moveBy(x: self.size.width + rocket.size.width, y: self.size.height, duration: TimeInterval(randomNumber(min: 3, max: 6)))
        let rotateastroids = SKAction.rotate(byAngle: CGFloat(M_PI), duration: TimeInterval(self.randomNumber(min: 1, max: 2)))
        let repeatRotateastroids = SKAction.repeat(rotateastroids, count: 3)
        let actionGroup = SKAction.group([runastroids, repeatRotateastroids])
        let removeastroids = SKAction.removeFromParent()
        self.moveAndRemoveastroids = SKAction.sequence([actionGroup, removeastroids])
        self.astroids.run(self.moveAndRemoveastroids)
    }
    
    //create score line, score increments when rocket passes through this line
    func createScoreLine(offset: CGFloat){
        self.scoreLine = SKSpriteNode(color: SKColor.clear, size: CGSize(width: 1000, height: 3))
        self.scoreLine.position =  CGPoint(x: self.size.width/2 - 70, y: self.size.height/2 + 400 + offset)
        self.scoreLine.zRotation = (45.0)
        
        self.scoreLine.zPosition = 5
        self.scoreLine.physicsBody = SKPhysicsBody(rectangleOf: scoreLine.size)
        self.scoreLine.physicsBody?.categoryBitMask = GameScene.scoreL
        self.scoreLine.physicsBody?.contactTestBitMask = GameScene.blackrocket
        self.scoreLine.physicsBody?.affectedByGravity = false
        self.scoreLine.physicsBody?.isDynamic = false
        self.scoreLine.name = "ScoreLine"
        self.addChild(self.scoreLine)
        self.nodeArray.append(self.scoreLine)
    }
    
    //return random number in range
    func randomNumber(min : Int, max : Int) -> Int {
        return Int(arc4random_uniform(UInt32(max - min))) + min
    }
    
    //create restart button
    func createRestartButton(){
        restartButton = SKSpriteNode(imageNamed : "restart")
        restartButton.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        restartButton.zPosition = 10
        restartButton.setScale(0)
        self.addChild(restartButton)
        restartButton.run(SKAction.scale(to: 0.5, duration: 0.3))
    }
    
   
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        let firstBody = contact.bodyA
        let secondBody = contact.bodyB
        if((firstBody.categoryBitMask == GameScene.astroids && secondBody.categoryBitMask == GameScene.blackrocket)||firstBody.categoryBitMask == GameScene.blackrocket && secondBody.categoryBitMask == GameScene.astroids){
              //stop all the child actions
             enumerateChildNodes(withName: "astroids", using: ({
                    (node, error) in
                    node.speed = 0
                    node.removeAllActions()
                    self.removeAllActions()
                }))
            
            enumerateChildNodes(withName: "ScoreLine", using: ({
                (node, error) in
                node.speed = 0
                node.removeAllActions()
            }))
            
            rocket.removeAllActions()
            scoreLine.removeAllActions()
            fire.removeAllActions()
            //animate explosion
            explosion = SKEmitterNode(fileNamed: "explosion.sks")!
            explosion.position = rocket.position
            explosion.zPosition = 5
            rocket.removeFromParent()
            fire.removeFromParent()

            self.addChild(explosion)
            self.run(SKAction.wait(forDuration: 1)){
            
                self.gameOver = true
                if(self.labelGenerated==false){
                    self.labelGenerated = true
                    self.createRestartButton()
                }
            }
            
        }
        if((firstBody.categoryBitMask == GameScene.scoreL && secondBody.categoryBitMask == GameScene.blackrocket)||firstBody.categoryBitMask == GameScene.blackrocket && secondBody.categoryBitMask == GameScene.scoreL){
            score += 1
            scoreLabel.text = "\(score)"
            if(score > highScore){
                highScore = score
                highScoreLabel.text = "BEST:\(highScore)"
                UserDefaults.standard.set(highScore, forKey: "highScore")
                UserDefaults.standard.synchronize()
            }
        }
        
    }
    //create score label
    func createScoreLabel(){
        scoreLabel.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2 + self.frame.height/2.5)
        scoreLabel.text = "\(score)"
        scoreLabel.fontName = "Star Jedi Outline"
        scoreLabel.fontColor = SKColor.white
        scoreLabel.zPosition = 5
        scoreLabel.fontSize = 50
        self.addChild(scoreLabel)
    }
    
    //create hight score label
    func createHighScoreLabel(){
        highScoreLabel.position = CGPoint(x: 80, y: self.frame.height/2 + self.frame.height/2.5)
        highScoreLabel.text = "BEST:\(highScore)"
        highScoreLabel.fontName = "Star Jedi Outline"
        highScoreLabel.fontColor = SKColor.white
        highScoreLabel.zPosition = 5
        highScoreLabel.fontSize = 30
        self.addChild(highScoreLabel)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //move scene when toughed
        self.rocket.run(moverocket)
        self.fire.run(moveFire)
        self.run(SKAction.wait(forDuration: 0.2)){
        for i in 0...self.nodeArray.count-1{
            self.nodeArray[i].run(self.moveScene)
            if(i==0){
                self.rocket.run(self.repositionrocket)
                self.fire.run(self.repositionFire)
            }
            if(self.nodeArray[i].position.y  <= -50){
                if(i==0){
                    self.nodeArray[i].position =  CGPoint(x: self.size.width/2 - 70, y: self.nodeArray[2].position.y + 760)
                }
                if(i==1){
                    self.nodeArray[i].position =  CGPoint(x: self.size.width/2 - 70, y: self.nodeArray[0].position.y + 760)
                }
                if(i==2){
                    self.nodeArray[i].position =  CGPoint(x: self.size.width/2 - 70, y: self.nodeArray[1].position.y + 760)
                }
            }
        }
        
        if(self.count==4){
            self.y_origin=500
            self.count = 0
        }
        else{
            self.y_origin-=300
            self.count+=1
        }
            
        self.enumerateChildNodes(withName: "astroids", using: ({
            (node, error) in
            node.run(self.moveScene)
        }))
        
        for touch in touches{
            let location  = touch.location(in: self)
            if(self.gameOver){
                if(self.restartButton.contains(location)){
                    self.restartGame()
                }
            }
        }
    }
    }
}

